﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroller : MonoBehaviour
{
    public float Speed;

    public Vector3 Offset;

    bool isOn = true;
    Transform tf;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            tf.Translate(Vector3.up * Speed * Time.deltaTime);
        }
    }
}
