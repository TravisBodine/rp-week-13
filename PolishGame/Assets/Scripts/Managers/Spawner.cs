﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public float spawnRadius;
    public float enemyDensity;
    public float ScoreDensity;

    public float spawnWidth;
    public float spawnDistance;
    public List<GameObject> Enemies;
    
 
    public List<GameObject> ScoreObjects;
    

    Transform Cameraview;

    float timerS = 0;
    float timerE = 0;


    // Start is called before the first frame update
    void Start()
    {
        Cameraview = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        SpawnEnemy();
        SpawnScoreObject();
    }

    private void SpawnScoreObject()
    {
        if(timerS < Time.time)
        {
            int index = Random.Range(0, ScoreObjects.Count - 1);

            GameObject obj = Instantiate(ScoreObjects[index], Random.insideUnitCircle * spawnRadius, Quaternion.identity);
            GameManager.instance.curScoreObjects.Add(obj);

            //need to change this later its spagetti
            obj.GetComponent<Flower>().spawner = this;
            timerS = Time.time + GameManager.instance.stats.ScoreObjectSpawnSpeed;
        }

    }

    private void SpawnEnemy()
    {
        if (timerE < Time.time) { 
            if (GameManager.instance.curEnemies.Count <= GameManager.instance.stats.MaxEnemies)
            {
                int index = Random.Range(0, Enemies.Count - 1);
                Vector3 bottom = new Vector3(Random.Range(-spawnWidth,spawnWidth), spawnDistance, 0);
                GameObject obj = Instantiate(Enemies[index], bottom, Quaternion.identity);
                GameManager.instance.curEnemies.Add(obj);
            }
            timerE = Time.time + GameManager.instance.stats.EnemySpawnDelay;
        }
    }
}
