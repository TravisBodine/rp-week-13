﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;

    public List<GameObject> menus;

    public void Start()
    {

        if (instance == null || instance.gameObject == null)
        {
            instance = this;
            GameManager.instance.menuManager = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void ChangeMenu(GameObject menuToChangeTo)
    {
        foreach (GameObject menu in menus)
        {
            if (menuToChangeTo == menu)
            {
                menu.SetActive(true);
            }
            else
            {
                menu.SetActive(false);
            }
        }
    }
    public void ChangeMenu(string menuToChangeTo)
    {
        foreach (GameObject menu in menus)
        {
            Debug.Log(menu.name);
            if (menuToChangeTo.Equals(menu.name))
            {
                menu.SetActive(true);
            }
            else
            {
                menu.SetActive(false);
            }
        }
    }

    public void LoadScene(string sceneToLoad)
    {
        
        GameManager.instance.ResetLevel();
        SceneManager.LoadScene(sceneToLoad);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
