﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Health : MonoBehaviour
{
    public int maxHP;

    public int curHP;

    public event Action OnDeath = delegate { };

    void Start()
    {
        curHP = maxHP;
    }
    public void ChangeHealth(int amount)
    {
        curHP += amount;

        if (curHP <= 0)
        {
            Death();
        }
    }

    public void Death()
    {
        OnDeath();
        Destroy(gameObject);
    }

}
