﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputHandler : MonoBehaviour
{
    [HideInInspector]
    public float VerticalAxis;
    [HideInInspector]
    public float HorizontalAxis;

    public event Action OnJumpButtonPressed = delegate { };
    public event Action OnFireButtonPressed = delegate { };
    public event Action OnEscapeButtonPressed = delegate { };

    // Update is called once per frame
    void Update()
    {
        VerticalAxis = Input.GetAxisRaw("Vertical");
        HorizontalAxis = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            OnJumpButtonPressed();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            OnFireButtonPressed();
        }


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // OnEscapeButtonPressed();
            //for now
            MenuManager.instance.LoadScene("Menus");
        }
    }
}
