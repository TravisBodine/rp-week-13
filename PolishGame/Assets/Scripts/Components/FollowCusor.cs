﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCusor : MonoBehaviour
{
    Transform tf;
    float CameraZOffset = -10;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = Input.mousePosition;
        target.z -= CameraZOffset;

        tf.position = Camera.main.ScreenToWorldPoint(target);
    }
}
