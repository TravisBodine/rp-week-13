﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionHandler : MonoBehaviour,Iinteractable
{
    public void interact(GameObject obj)
    {
        QueenBee player = obj.GetComponent<QueenBee>();
        if (player != null)
        {
            if(player.Bees.Count > 1)
            {
                GetComponent<Health>().ChangeHealth(-1);
                player.killBee();
            }
           
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Hive hive = collision.gameObject.GetComponent<Hive>();
        //if(hive != null)
        //{
        //    hive.OnAttacked();
        //}
    }
}
