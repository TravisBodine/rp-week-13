﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : MonoBehaviour
{

    public float Range;
    public float turnSpeed;
    public float DelayTime;
    public float FireTime;
    public float FireAngle;

    public float ParticalMultiplier;
    public Transform Gun;

    public enum FlameThrowerStates { seeking,charging,destorying};
    FlameThrowerStates _curState;

    Transform tf;
    ParticleSystem FlameParticleEffect;
    public Vector3? target;

    public BoxCollider2D FlameHitBox;


    float timer;
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();

        FlameParticleEffect = GetComponentInChildren<ParticleSystem>();
        FlameParticleEffect.Stop();
        //FlameParticleEffect.startSpeed = Range * ParticalMultiplier;

        FlameHitBox = GetComponentInChildren<BoxCollider2D>();
        FlameHitBox.size = new Vector2(FlameHitBox.size.x,Range);
        FlameHitBox.gameObject.SetActive(false);

        _curState = FlameThrowerStates.seeking;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_curState)
        {
            case FlameThrowerStates.seeking:
                CheckForTargets();
                break;
            case FlameThrowerStates.charging:
                LookAtTarget(target);
                break;
            case FlameThrowerStates.destorying:
                break;

        }
    }

    void CheckForTargets()
    {
        target = null;
        float _distFromPlayer = Vector3.Distance(GameManager.instance.queenBee.gameObject.transform.position, tf.position);
        if(_distFromPlayer <= Range)
        {
            target = GameManager.instance.queenBee.gameObject.transform.position;
            LookAtTarget(target);
            _curState = FlameThrowerStates.charging;
           
        }
        else
        {
            
            foreach (GameObject obj in GameManager.instance.curScoreObjects)
            {
                float _distFromobj = Vector3.Distance(obj.transform.position, tf.position);
             
                if (_distFromobj <= Range)
                {
                    target = obj.transform.position;
                    LookAtTarget(target);
                    _curState = FlameThrowerStates.charging;
                }
              
            }
           
        }
        
    }

    void LookAtTarget(Vector3? target)
    {
        if (target == null)
            return;

            Vector3 VectorToTarget = (Vector3)(tf.position - target);
            float zRotaion = Mathf.Atan2(VectorToTarget.y, VectorToTarget.x) * Mathf.Rad2Deg;

            Quaternion lookRotation = Quaternion.Euler(0f, 0f, zRotaion + (90));

            Gun.rotation = Quaternion.RotateTowards(Gun.rotation, lookRotation, turnSpeed * Time.deltaTime);

            
            if (Vector3.Angle(Gun.forward, VectorToTarget) - 90 < FireAngle)
            {

                if (timer <= Time.time)
                {
                     StartCoroutine(FireGun());

                }
            }
    }
     
    IEnumerator FireGun()
    {
        float fireTimer = 0;
        FlameParticleEffect.Play();
        FlameHitBox.gameObject.SetActive(true);

        while(fireTimer <=FireTime)
        {
            FlameHitBox.size = new Vector2(FlameHitBox.size.x, (fireTimer / FireTime) * Range);

            FlameHitBox.offset = new Vector2(0, Mathf.Abs(FlameHitBox.size.y/2));

            fireTimer += Time.deltaTime;
            yield return null;
        }

        FlameParticleEffect.Stop();
        FlameParticleEffect.Clear();
        FlameHitBox.gameObject.SetActive(false);
        timer = Time.time + DelayTime;
        _curState = FlameThrowerStates.seeking;
    }

}
