﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller_Enemy : MonoBehaviour
{
    pawn_Enemy pawn;
    Flamethrower therealcontroller;

    public float offscreenDist;
    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<pawn_Enemy>();
        therealcontroller = GetComponentInChildren<Flamethrower>();
    }

    // Update is called once per frame
    void Update()
    {

        if(pawn.transform.position.x> offscreenDist)
        {
            Destroy(gameObject);
        }
  
        pawn.MoveForward();

       
    }
}
