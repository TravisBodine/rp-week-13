﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pawn_Enemy : MonoBehaviour
{
   

    public float Speed;

    Vector3 Target;

    Transform tf;


    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    public void MoveForward()
    {
        tf.Translate(Vector3.up * Speed * Time.deltaTime);
    }

    public void RotateToLookAt(Vector3 targetPosition)
    {
        Vector3 vectorToTarget = Vector3.zero - tf.position;

        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x);
        transform.rotation = Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg - 90);

    }

    void OnDestroy()
    {
        GameManager.instance.curEnemies.Remove(gameObject);
    }
}
