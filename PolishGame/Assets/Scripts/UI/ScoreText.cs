﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    public string preScoreText = "Score : ";
    Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        ScoreHandler.OnScoreUpdated += UpdateScore;
    }

    void UpdateScore()
    {
        text.text = preScoreText + GameManager.instance.CurScore;
    }

    private void OnDisable()
    {
        ScoreHandler.OnScoreUpdated -= UpdateScore;
    }
}
