﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public float smoothing;

    public float MaxDist = 10;

    Vector3 offset = new Vector3(0, 0, -10);

    Transform tf;

    private void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void  LateUpdate()
    {
        
        Vector3 targetPosition = new Vector3(Mathf.Clamp(GameManager.instance.queenBee.gameObject.transform.position.x,-MaxDist, MaxDist), Mathf.Clamp(GameManager.instance.queenBee.gameObject.transform.position.y, -MaxDist, MaxDist),0);
        tf.position = Vector3.Lerp(tf.position,targetPosition + offset, smoothing * Time.deltaTime);
    }
}
