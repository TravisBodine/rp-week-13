﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hive : MonoBehaviour
{

    

    LoadingBar beeProgressBar;

    float timer = 0;
    bool isLoading = false;
    // Start is called before the first frame update
    void Start()
    {
        beeProgressBar = GetComponentInChildren<LoadingBar>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLoading)
        {
            if (timer >= 0)
            {
                timer -= Time.deltaTime;
                beeProgressBar.UpdateBar(timer, GameManager.instance.stats.BeeSpawnSpeed);
            }
        }
    }

    public void OnAttacked()
    {
        GameManager.instance.HandleLoss();
    }

 

    private void OnMouseEnter()
    {
        isLoading = true;
    }

    void OnMouseOver()
    {
        if(timer < GameManager.instance.stats.BeeSpawnSpeed)
            timer += Time.deltaTime;

        if (timer >= GameManager.instance.stats.BeeSpawnSpeed)
        {
            if(GameManager.instance.queenBee.Bees.Count < GameManager.instance.stats.MaxBees)
            {
                GameManager.instance.queenBee.SpawnBee();
                timer = 0;
            }
            
        }
        beeProgressBar.UpdateBar(timer, GameManager.instance.stats.BeeSpawnSpeed);

    }
    private void OnMouseExit()
    {
        isLoading = false;
    }
}
