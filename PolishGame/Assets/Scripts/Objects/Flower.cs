﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour, Iinteractable
{
    [SerializeField]
    float GrowTime;

    [SerializeField]
    List<Sprite> sprites;

    [SerializeField]
    float TimeOutTime;

    public Spawner spawner;

    SpriteRenderer renderer;

    ScoreHandler score;
    bool active = false;
    

    // Start is called before the first frame update
    void Start()
    {
        score = GetComponent<ScoreHandler>();
        renderer = GetComponentInChildren<SpriteRenderer>();

        StartCoroutine(Grow());
    }

    IEnumerator Grow()
    {
        float timer = 0;
        int spriteIndex = 0;
        while (timer <= GrowTime +.1f)
        {

            float temp = (float)spriteIndex / ((float)sprites.Count - 1f);
        
            if (timer/GrowTime > temp)
            {

                if(spriteIndex <= sprites.Count-1)
                    renderer.sprite = sprites[spriteIndex];
                spriteIndex++;
               
            }
            timer += Time.deltaTime;
            yield return null;
        }

        //renderer.sprite = sprites[sprites.Count-1];
        active = true;
        Destroy(gameObject, TimeOutTime);
    }

    public void interact(GameObject obj)
    {
        Debug.Log(obj.name);
        QueenBee player = obj.GetComponent<QueenBee>();
        if (player != null)
        {
            if (active)
            {
                score?.AddScore();

                GameManager.instance.queenBee.SpawnBee();
                //spawner.curScoreObjects.Remove(this.gameObject);
                Destroy(gameObject);
            }
        }

        FireCollisionHandler enemy = obj.GetComponent<FireCollisionHandler>();
        if (enemy != null)
        {
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        
        GameManager.instance.curScoreObjects.Remove(this.gameObject);
    }
}
